package sql

import org.apache.spark.sql.SparkSession

/**
  * Created by mark on 04/06/2017.
  */

/**
  請以SparkSQL完成以下題目(70%)
 */
object SparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

/**
1. 根據以下題目敘述，完成???部份 (10%)
*/
  /**
  1.1 請讀取dataset/small-ratings.csv, 並創建一個名為ratings的TempView(Table) (5%)

    */

  var ratingsSrc=ss.read.option("header", "true").csv(???)

  ratingsSrc.createOrReplaceTempView(???)

  /**
  1.2 請讀取dataset/movies.csv, 並創建一個名為movies的TempView(Table) (5%)

    */

  var moviesSrc=ss.read.option("header", "true").csv(???)

  moviesSrc.createOrReplaceTempView(???)

/**
2. 根據以下題目敘述，使用SQL語法完成以下???部份(60%)
*/
  /**
  2.1 試著從small-ratings.csv中，根據timestamp做降序排列，並show與以下欄位相同的資料格式(10%)
  +------+-------+------+----------+
  |userId|movieId|rating| timestamp|
  +------+-------+------+----------+
  */
  ss.sql("SELECT * FROM ratings ORDER BY timestamp DESC").show()

  /**
  2.2 試著從small-ratings.csv中，計算每部電影的平均rating, 並show與以欄位相同的資料(10%)
  +-------+------------------+
  |movieId|              mean|
  +-------+------------------+

    */
  ss.sql("SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId").show()

  /**
  2.3 試著從small-ratings.csv中，計算出每部電影平均rating並根據平均rating做降序排列，
  並show與以下欄位相同的資料格式(10%)
  +-------+------------------+
  |movieId|              mean|
  +-------+------------------+

    */
  ss.sql("SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId ORDER BY avg(rating)").show()
  /**
  2.4 試著從small-ratings.csv中，找出movieId為30707且userId為107799，並show與以下欄位相同的資料格式(10%)
  +------+-------+------+----------+
  |userId|movieId|rating| timestamp|
  +------+-------+------+----------+


    */
  ss.sql("SELECT * FROM ratings WHERE movieId=30707 AND userId=107799 ").show()
  /**
  2.5 試著從movie.csv中，找出movieId為10的資料, 並show與以欄位相同的資料(10%)
  +-------+------------------+
  |movieId|              title|
  +-------+------------------+


    */
  ss.sql("SELECT movieId,title FROM movies WHERE movieId=10 GROUP BY movieId ").show()

/**
  2.6 試著從small-ratings.csv及movies.csv兩張表Join起來，以timestamp降序排列，
  並show出與以下欄位相同的資料格式(10%)
  +-------+--------------------+------+---------+
  |movieId|               title|rating|timestamp|
  +-------+--------------------+------+---------+
  */
  ss.sql("SELECT m.movieId,m.title,rating,timestamp\nfrom movies as m join ratings as r\nWHERE m.movieId=r.movieId\nORDER BY timestamp DESC ").show()

}
